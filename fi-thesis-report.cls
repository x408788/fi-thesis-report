% fi-thesis-report.sty
% LaTeX style for theses reports at FI MU
% Released to public domain.
% Feel free to contribute or add bug reports in the form of issues
% in the project's repository at https://gitlab.fi.muni.cz/xukrop/fi-thesis-report/
\ProvidesClass{fi-thesis-report}[2017/06/09 v1.2 FI thesis report template]

\newcommand{\ftr@colorProfile}{color}
\DeclareOption{bw}{\renewcommand{\ftr@colorProfile}{bw}}
\DeclareOption{color}{} % the default

\newcommand{\ftr@lang}{czech}
\DeclareOption{english}{\renewcommand{\ftr@lang}{english}}
\DeclareOption{slovak}{\renewcommand{\ftr@lang}{slovak}}
\DeclareOption{czech}{} % the default

% Based on 'article'
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions \relax
\LoadClass{article}

% Load necessary packages
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{bera}
\RequirePackage{charter}
\RequirePackage{eulervm}
\RequirePackage[a4paper, top=20mm, bottom=25mm, left=20mm, right=20mm]{geometry}
\RequirePackage[final,kerning=true,spacing=true]{microtype}
\microtypecontext{spacing=nonfrench}
\RequirePackage[\ftr@lang]{babel}
\RequirePackage{color}
\RequirePackage{graphicx}
\RequirePackage{tabularx}
\RequirePackage{ifthen}
\RequirePackage{tikz}
\usetikzlibrary{positioning}

% Define colours
\definecolor{blue-mu}{RGB}{0,0,220}
\ifthenelse{\equal{\ftr@colorProfile}{bw}}{
\definecolor{ref-dark-red}{rgb}{0.3,0.3,0.3}
\definecolor{ref-dark-green}{rgb}{0.3,0.3,0.3}
\definecolor{ref-medium-blue}{rgb}{0.3,0.3,0.3}
}{
\definecolor{ref-dark-red}{rgb}{0.6,0.15,0.15}
\definecolor{ref-dark-green}{rgb}{0.15,0.4,0.15}
\definecolor{ref-medium-blue}{rgb}{0,0,0.5}
}

% setup hyperref
\RequirePackage[ colorlinks=true
               , linkcolor={ref-dark-red}
               , citecolor={ref-dark-green}
               , urlcolor={ref-medium-blue}
               ]{hyperref}

% Define name strings and setting commands
\newcommand{\setColorProfile}[1]{\def\ftr@colorProfile{#1}%
    \PackageWarning{fi-thesis-report}{setColorProfile is deprecated, please use bw documentclass option (colorprofile: bw for markdown)}}
\def\@reportType{}
\newcommand{\setReportType}[1]{\def\@reportType{#1}}
\def\@thesisType{}
\newcommand{\setThesisType}[1]{\def\@thesisType{#1}}
\def\@thesisName{}
\newcommand{\setThesisName}[1]{\def\@thesisName{#1}}
\def\@student{}
\newcommand{\setStudent}[1]{\def\@student{#1}}
\newcommand{\setSupervisor}[1]{\def\@supervisor{#1}}
\newcommand{\setConsultant}[1]{\def\@consultant{#1}}
\newcommand{\setReader}[1]{\def\@reader{#1}}
\newcommand{\setInterReader}[1]{\def\@interReader{#1}}
\def\@signature{%
\ifthenelse{\equal{\@reportType}{inter-reader}}{\@interReader}{%
\ifthenelse{\equal{\@reportType}{reader}}{\@reader}{%
\ifthenelse{\equal{\@reportType}{consultant}}{\@consultant}{\@supervisor}}}%
}
\newcommand{\setSignature}[1]{\def\@signature{#1}}

% Helper for translation
\newcommand{\ftr@ite@lang}[3]{\ifthenelse{\equal{\ftr@lang}{czech}}{#1}{\ifthenelse{\equal{\ftr@lang}{slovak}}{#2}{#3}}}

% Determine correct title texts
\ftr@ite@lang{
\def\@titleFirst{Posudek
\ifthenelse{\equal{\@reportType}{inter-reader}}{interního oponenta}{%
\ifthenelse{\equal{\@reportType}{reader}}{oponenta}{%
\ifthenelse{\equal{\@reportType}{consultant}}{konzultanta}{vedoucího}}}
}
\def\@titleSecond{%
\ifthenelse{\equal{\@thesisType}{phd}}{disertační}{%
\ifthenelse{\equal{\@thesisType}{bc}}{bakalářské}{diplomové}%
} práce}
}{
\def\@titleFirst{Posudok
\ifthenelse{\equal{\@reportType}{inter-reader}}{interného oponenta}{%
\ifthenelse{\equal{\@reportType}{reader}}{oponenta}{%
\ifthenelse{\equal{\@reportType}{consultant}}{konzultanta}{vedúceho}}}
}
\def\@titleSecond{%
\ifthenelse{\equal{\@thesisType}{phd}}{disertačnej}{%
\ifthenelse{\equal{\@thesisType}{bc}}{bakalárskej}{diplomovej}%
} práce}
}{
\def\@titleFirst{%
\ifthenelse{\equal{\@thesisType}{phd}}{Disertation}{%
\ifthenelse{\equal{\@thesisType}{bc}}{Bachelor}{Master}%
} Thesis}
\def\@titleSecond{%
\ifthenelse{\equal{\@reportType}{inter-reader}}{Internal}{%
\ifthenelse{\equal{\@reportType}{reader}}{Reader's}{%
\ifthenelse{\equal{\@reportType}{consultant}}{Consultant's}{Supervisor's}}
} Review}
}

\newcommand{\markA}{\ftr@ite@lang{výborně}{výborne}{excellent} (A)}
\newcommand{\markB}{\ftr@ite@lang{velmi dobře}{veľmi dobre}{very good} (B)}
\newcommand{\markC}{\ftr@ite@lang{dobře}{dobre}{good} (C)}
\newcommand{\markD}{\ftr@ite@lang{uspokojivě}{uspokojivo}{satisfactory} (D)}
\newcommand{\markE}{\ftr@ite@lang{vyhovující}{vyhovujúci}{sufficient} (E)}
\newcommand{\markF}{\ftr@ite@lang{nevyhovující}{nevyhovujúci}{failed} (F)}

% Document header (logo + table with names)
\AtBeginDocument{
\begin{tikzpicture}[remember picture,overlay]
\node[] (TopLeft) at (current page.north west) {};
\node[anchor=north west](FI-logo) [below right=11.5mm and 14mm of TopLeft] {%
\ifthenelse{\equal{\ftr@colorProfile}{bw}}%
{\ftr@ite@lang{\includegraphics[scale=0.6]{logos/fi-cz-bw}}{\includegraphics[scale=0.6]{logos/fi-cz-bw}}{\includegraphics[scale=0.6]{logos/fi-en-bw}}}%
{\ftr@ite@lang{\includegraphics[scale=0.6]{logos/fi-cz-color}}{\includegraphics[scale=0.6]{logos/fi-cz-color}}{\includegraphics[scale=0.6]{logos/fi-en-color}}}%
};

\node[] (TopRight) at (current page.north east) {};
\node[below left=14.3mm and 16.5mm of TopRight](posudek){{\large \textsc{\@titleFirst}}};
\node[below=4.9mm of posudek.east,anchor=east]{{\large \textsc{\@titleSecond}}};
\end{tikzpicture}
\vspace*{13mm}

\noindent
{\def\arraystretch{1.3}\tabcolsep=10pt
\begin{tabularx}{\textwidth}{@{} l >{\it} X @{}}
\ftr@ite@lang{Název práce}{Názov práce}{Title}: & \@thesisName \\
\ftr@ite@lang{Autor práce}{Autor práce}{Author}: & \@student
\ifthenelse{\isundefined{\@supervisor}}{}{\\\ftr@ite@lang{Vedoucí práce}{Vedúci práce}{Supervisor}: & \@supervisor}%
\ifthenelse{\isundefined{\@consultant}}{}{\\\ftr@ite@lang{Konzultant}{Konzultant}{Consultant}: & \@consultant}%
\ifthenelse{\isundefined{\@reader}}{}{\\\ftr@ite@lang{Oponent}{Oponent}{Reader}: & \@reader}%
\ifthenelse{\isundefined{\@interReader}}{}{\\\ftr@ite@lang{Interní oponent}{Interný oponent}{Internal Reader}: & \@interReader}%
\end{tabularx}
}
\vspace{-0.5em}
\par\noindent
{%
\ifthenelse{\equal{\ftr@colorProfile}{bw}}{}{\color{blue-mu}}%
\rule{\textwidth}{0.3mm}
}
\vspace{-0.5em}
\par\@afterindentfalse\@afterheading
}

% Document footer (date, signature)
\AtEndDocument{
\vspace{2em}
\noindent
Brno \@date \hfill \@signature
}
