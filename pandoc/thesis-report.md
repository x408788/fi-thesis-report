---
colorprofile: color  # 'color' or 'bw'
language: czech      # 'czech' or 'english'
reporttype: reader   # 'reader' or 'consultant' or 'supervisor' or 'inter-reader'
thesistype: bc       # 'bc' or 'mgr' or 'phd'
thesisname: The Great Thesis
student: John Doe
supervisor: Mgr. Richard Roe, Ph.D.  # comment to hide
consultant: Ing. Maggie Lane         # comment to hide
reader: RNDr. John Smith             # comment to hide
inter-reader: RNDr.\ Mary Poppins, Ph.D.    # comment to hide
#signature: John Smith               # uncomment for custom signature
#date: 1\. 4. 2020                   # uncomment for custom date
# Note: Escaping the the first '.' via '\.' is necessary, otherwise pandoc
#       gets confused and assumes this is an enumerated list.
header-includes:
    - \usepackage{lipsum}
...

\lipsum

S přihlédnutím k celkovému dosaženému výsledku navrhuji hodnotit práci známkou velmi dobře (B).
