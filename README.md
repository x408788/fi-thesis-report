FI thesis report
================

Unofficial LaTeX template for thesis reports at FI MU. Uses the new university visual style.

* You can find the project repository on [FI MUNI GitLab](https://gitlab.fi.muni.cz/teaching-lab/fi-thesis-report).
* If you identify any problems, file it in the [issue tracker](https://gitlab.fi.muni.cz/teaching-lab/fi-thesis-report/issues).
* The template is also available in the [Overleaf gallery](https://www.overleaf.com/latex/templates/thesis-report-for-the-faculty-of-informatics-at-the-masaryk-university-in-brno/wjdtfzqpcvch).

## Usage

* The project defines a new LaTeX document style 'fi-thesis-report' based on the traditional TeX class 'article'.
* The easiest way how to use the existing example file *thesis-report.tex*. Documentation of the important commands is provided in comments.
* To build the report correctly, it is necessary to **run the build twice** (faculty logo is included as PDF).
* The included Makefile builds all '.tex' files in the current directory (so it is convenient to store multiple reports right there).

## Tips and tricks

* All options passed to 'fi-thesis-report' are passed to the underlying 'article' class.
* If you prefer pandoc and markdown, see `pandoc/`
